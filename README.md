chicken-test
-----------
Forked from Yet Another Testing Utility egg by Alex Shinn for Chicken Scheme.

Patches
-------

* Changed [ PASS] to [PASS]
    - I did this because it always seems broken or not properly indented.

Getting Started
---------------

Installing
----------
Run `chicken-install test.setup`

Authors
------
* faraco
* Alex Shinn

License
-------
This project is licensed under the BSD-3-Clause license. See the LICENSE file for details.
